﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    [SerializeField] CharacterController controller;
    [SerializeField] float minSpeed = 5f;
    float speed = 5f;
    [SerializeField] float maxSpeed = 10f;
    [SerializeField] float gravity = -9.81f;
    [SerializeField] Transform groundCheck;
    [SerializeField] float groundDistance = 0.1f;
    [SerializeField] LayerMask playerMask;
    Vector3 move;
    Vector3 velocity;
    bool isGrounded;
    float jumpHeight = 1f;
    float jumpTime = 0f;
    float jumpCooldown = 0.1f;
    float jumpCountdown = 0.3f;
    

    void StepOffsetFix()
    {
        if (isGrounded)
        {
            controller.stepOffset = 0.1f;

            if (velocity.y < 0)
            {
                velocity.y = -2f;
            }
        }
        else
        {
            controller.stepOffset = 0;
        }
    }

    // Update is called once per frame
    void Update()
    {
        isGrounded = Physics.CheckSphere(groundCheck.position, groundDistance, playerMask);

        StepOffsetFix();

        if (jumpCooldown > 0)
        {
            jumpCooldown -= Time.deltaTime;
        }

        if (isGrounded)
        {
            jumpTime += Time.deltaTime;

            if (Input.GetButton("Jump") && jumpCooldown <= 0)
            {
                if (speed < maxSpeed)
                {
                    speed += 1f;
                }
                velocity.y = Mathf.Sqrt(jumpHeight * -2f * gravity);
                jumpTime = 0;
                jumpCooldown = 0.1f;
            }

            if (jumpTime >= jumpCountdown)
            {
                if (speed > minSpeed)
                {
                    speed -= 1f;
                }
                jumpTime = 0;
            }
        }

        float x = Input.GetAxis("Horizontal");
        float z = Input.GetAxis("Vertical");

        move = transform.right * x + transform.forward * z;

        controller.Move(move * speed * Time.deltaTime);

        velocity.y += gravity * Time.deltaTime;
        controller.Move(velocity * Time.deltaTime);
    }
}
