﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseLook : MonoBehaviour
{
    float mouseSensitivity = 60f;
    float padSensitivity = 900f;
    [SerializeField] Transform playerBody;
    float xRotation = 0f;

    // Start is called before the first frame update
    void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
    }

    // Update is called once per frame
    void Update()
    {
        float mouseX = Input.GetAxis("Mouse X") * mouseSensitivity * Time.deltaTime;
        float mouseY = Input.GetAxis("Mouse Y") * mouseSensitivity * Time.deltaTime;
        float rightStickX = Input.GetAxis("RightStick X") * padSensitivity * Time.deltaTime;
        float rightStickY = Input.GetAxis("RightStick Y") * padSensitivity * Time.deltaTime;

        xRotation -= mouseY;
        xRotation -= rightStickY;
        
        xRotation = Mathf.Clamp(xRotation, -90f, 90f);

        transform.localRotation = Quaternion.Euler(xRotation, 0f, 0f);
        playerBody.Rotate(Vector3.up * mouseX);
        playerBody.Rotate(Vector2.up * rightStickX);
    }
}
